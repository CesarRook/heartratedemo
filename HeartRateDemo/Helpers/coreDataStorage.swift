//
//  coreDataStorage.swift
//  HeartRateDemo
//
//  Created by Cesar Rook on 30/04/20.
//  Copyright © 2020 Cesar Rook. All rights reserved.
//

import Foundation


public class HRMeassure: NSObject, NSCoding{
    public var sessionArray : [NSNumber] = []
    
    enum key : String{
        case hrDataSession = "hrDataSession"
    }
    
    init(sessionArray: [NSNumber]) {
        self.sessionArray = sessionArray
    }

    public func encode(with coder: NSCoder) {
        coder.encode(sessionArray, forKey: key.hrDataSession.rawValue)
    }
    
    public required convenience init?(coder: NSCoder) {
        let mSessionData = coder.decodeObject(forKey: key.hrDataSession.rawValue) as! [NSNumber]
        
        self.init(sessionArray: mSessionData)
    }
    
}
