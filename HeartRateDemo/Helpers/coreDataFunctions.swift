//
//  coreDataFunctions.swift
//  HeartRateDemo
//
//  Created by Cesar Rook on 30/04/20.
//  Copyright © 2020 Cesar Rook. All rights reserved.
//

import Foundation
import UIKit
import CoreData


//MARK: Funcion para guardar datos en la Base de Datos
public func createData( sessionArray : [NSNumber]) {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
    let managedContext = appDelegate.persistentContainer.viewContext
    let userEntity = NSEntityDescription.entity(forEntityName: "HREntity", in: managedContext)!
    
    
    let hrEntity = NSManagedObject(entity: userEntity, insertInto: managedContext) as! HREntity
    let mArray = HRMeassure(sessionArray: sessionArray)
    hrEntity.setValue(mArray, forKeyPath: "hrData")
    
    do{
        try managedContext.save()
    } catch let error as NSError{
        print("No se pudo guardar . \(error), \(error.userInfo)")
    }
}

//MARK: Funcion para cargar los datos guardados en la base de datos
public func retrieveData() -> [NSNumber] {
    var arrayToReturn: [NSNumber]
    arrayToReturn = [0,0,0,0]
    
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return [0]}
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "HREntity")
    
    
    do{
        let result = try managedContext.fetch(fetchRequest)
        
        for data in result as! [NSManagedObject]{
            let mRanges = data.value(forKey: "hrData") as! HRMeassure
            
            //print(mRanges.sessionArray)
            arrayToReturn = mRanges.sessionArray
            print("Regresamos esto: \(arrayToReturn)")
        }
        
    } catch let error as NSError{
        print("No se pudo cargar . \(error), \(error.userInfo)")
        
    }
    
    return arrayToReturn
}
