//
//  ViewController.swift
//  HeartRateDemo
//
//  Created by Cesar Rook on 29/04/20.
//  Copyright © 2020 Cesar Rook. All rights reserved.
//

import UIKit
import CoreBluetooth

//Variable para localizar el dispositivo por su UUID segun la documentación es 180D para HR
let heartRateServiceCBUUID = CBUUID(string: "0x180D")
let heartRateMeasurementCharacteristicCBUUID = CBUUID(string: "2A37")
let bodySensorLocationCharacteristicCBUUID = CBUUID(string: "2A38")
let commonNotify = CBUUID(string: "FD00")
let common_19 = CBUUID(string: "FD19")
let common_write = CBUUID(string: "FD1A")


//Arreglo donde guardaremos los datos de la session
var sessionHR : [NSNumber] = []

class HRMViewController: UIViewController {

  @IBOutlet weak var heartRateLabel: UILabel!
  @IBOutlet weak var bodySensorLocationLabel: UILabel!
    @IBOutlet weak var savedDataLbl: UILabel!
    
    //Variable con la que manejarémos a central
    var centralManager: CBCentralManager!
    //Variable para manejar la referencia de nuestro dispositivo periferico
    var heartRatePeripheral: CBPeripheral!
    //Variable para guardar la caracteristica a escribir
    var caract_common_write : CBCharacteristic!
    //Variable donde guardaremos la primera lectura
    var firtStepsLecture = 0
    //Variable para desplegar la lógica de la primera lectura
    var firstLecture = false


  override func viewDidLoad() {
    super.viewDidLoad()
    

    if #available(iOS 13.0, *) {
        overrideUserInterfaceStyle = .light
    } else {
        // Fallback on earlier versions
    }
    
    //Inicializamos la variable, tanto delegado como su queue pero en este caso no aplica.
    centralManager = CBCentralManager(delegate: self, queue: nil)

    //añadimos formato de fuente y estílo a nuestro label
    heartRateLabel.font = UIFont.monospacedDigitSystemFont(ofSize: heartRateLabel.font!.pointSize, weight: .regular)
  }

//MARK: Función para mostrar el HR obtenido en el label
  func onHeartRateReceived(_ heartRate: Int) {
    //Guardamos el dato en el array de la session
    sessionHR.append(NSNumber(value: heartRate))
    //print("Array hasta ahora:_ \(sessionHR)")
    //Mostramos el valor en el label
    heartRateLabel.text = String(heartRate)
    print("BPM: \(heartRate)")
    //Se imprime cada vez que se obtenga la frecuencia, el erreglo qu tenemos
    let obtainedCoreDataSavedArrayStringValue = sessionHR.map { $0.stringValue }
    savedDataLbl.text = obtainedCoreDataSavedArrayStringValue.joined(separator: ",")
  }
    
//MARK: Actions de Botones
    @IBAction func saveBtnTapped(_ sender: Any) {
        createData(sessionArray: sessionHR)
    }
    
    @IBAction func fetchBtnTapped(_ sender: Any) {
        let obtainedCoreDataSavedArray = retrieveData()
        let obtainedCoreDataSavedArrayStringValue = obtainedCoreDataSavedArray.map { $0.stringValue }
        
        savedDataLbl.text = obtainedCoreDataSavedArrayStringValue.joined(separator: ",")
    }
    
}

//MARK: En esta extensión del controlador haremos lo referente al bluetooth central
extension HRMViewController: CBCentralManagerDelegate {
    
    //Funcion autogenerada por el Delegado
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        //En este switch reconocerémos el estado del central y vamos a manejar cada uno de ellos
        switch central.state {
          case .unknown:
            print("central.state is .unknown")
          case .resetting:
            print("central.state is .resetting")
          case .unsupported:
            print("central.state is .unsupported")
          case .unauthorized:
            print("central.state is .unauthorized")
          case .poweredOff:
            print("central.state is .poweredOff")
          case .poweredOn:
            //Si esta el BT encendido entonces escaneamos dispositivos
            print("central.state is .poweredOn")
            //Escaneamos solamente perifericos con UUID 1800D
            centralManager.scanForPeripherals(withServices: [heartRateServiceCBUUID], options: nil)
        @unknown default:
            fatalError()
        }
    }
    
    //Funcion para saber si ya encontramos un periferico
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
            print(peripheral)
        //Una vez encontrado lo asignamos a nuestra variable local
        heartRatePeripheral = peripheral
        //Añadimos el delegado para poder implementar los metodos de la extension
        heartRatePeripheral.delegate = self
        //Detenemos el escaneo de dispositivos
        centralManager.stopScan()
        //Ahora nos conectaremos al periferico
        centralManager.connect(heartRatePeripheral, options: nil)
    }
    
    //Funcion para saber si ya nos conectamos al periferíco y una vez conectados recibir datos
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Conectado!")
        //Verificamos los servicios que nos ofrece el periferico con esta linea de código
        //heartRatePeripheral.discoverServices(nil)
        //Verificamos si hay servicio de HeartRate con la siguiente linea
        heartRatePeripheral.discoverServices([commonNotify,heartRateServiceCBUUID])
        //heartRatePeripheral.discoverServices([heartRateServiceCBUUID])

    }
}//END Extension Central Maganer Delegate


//MARK: Extension usada para los delegados del periférico
extension HRMViewController: CBPeripheralDelegate {
    
    //Funcion para saber los servicios que nos ofrece el periferico
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        // Si hay servicios seguimos el algoritmo, si no salimos de la función
        guard let services = peripheral.services else { return }

        //Para todos los servicios encontrados los imprimimos
        for service in services {
            print("___Servicios___")
            print(service)
            //Imprimimos las características de el servicio encontrado
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    //Delegado para saber las características del servicio del periférico
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService,
                    error: Error?) {
        //Si hay características pasamos a la siguiente línea
        guard let characteristics = service.characteristics else { return }
        
        //Imprimimos todas las características que tenga ese servicio
        for characteristic in characteristics {
            print("___Caracteristicas____")
            print(characteristic)
            print("___Propiedades____")
            
            //Verificamos la característica de la propiedad si la contiene .read y .notify
            if characteristic.properties.contains(.read) {
              print("\(characteristic.uuid): properties contains .read")
              peripheral.readValue(for: characteristic)
            }
            if characteristic.properties.contains(.notify) {
              print("\(characteristic.uuid): properties contains .notify")
                peripheral.setNotifyValue(true, for: characteristic)
            }
            if characteristic.properties.contains(.writeWithoutResponse) {
                print("\(characteristic.uuid): properties contains .write")
                if characteristic.uuid == common_write {
                    print("Se encontro \(characteristic.uuid)")
                    caract_common_write = characteristic
                }
            }
        }
    }
    
    //Funcion para que el periferico nos mande valores
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic,
                    error: Error?) {
        
        print("ESTO_ENTRA: \(characteristic)")
        
        
      switch characteristic.uuid {
        case bodySensorLocationCharacteristicCBUUID:
          let bodySensorLocation = bodyLocation(from: characteristic)
          bodySensorLocationLabel.text = bodySensorLocation
        case heartRateMeasurementCharacteristicCBUUID:
            let bpm = heartRate(from: characteristic)
            onHeartRateReceived(bpm)
        case common_19:
            print("Handle_FD19")
            print(characteristic)
            print(characteristic.value)
            validateAcknowledgeResponse(from: characteristic)
            
            //stepsData(from: caract)
        case common_write:
            enableStepsNotification(from: characteristic)
            print("Segun yo aqui estamos en FD1A")
            print(characteristic)

        default:
          print("Unhandled Characteristic UUID: \(characteristic.uuid)")
      }
    }

    
    //Funcion para saber la parte del cuerpo que cubre el sensor
    private func bodyLocation(from characteristic: CBCharacteristic) -> String {
      guard let characteristicData = characteristic.value,
        let byte = characteristicData.first else { return "Error" }

        switch byte {
            case 0: return "otro"
            case 1: return "Pecho"
            case 2: return "Muñeca"
            case 3: return "Dedo"
            case 4: return "Mano"
            case 5: return "Oreja"
            case 6: return "Pie"
            default:
                return "Reservados para usos futuros"
        }
    }

//MARK: Funcion para obtener los latidos del corazon basado en los bits de un entero uint 8 que puede ir hasta 255 rpm
    
    private func heartRate(from characteristic: CBCharacteristic) -> Int {
        guard let characteristicData = characteristic.value else { return -1 }
        let byteArray = [UInt8](characteristicData)

        let firstBitValue = byteArray[0] & 0x01
        if firstBitValue == 0 {
            // Heart Rate Value Format is in the 2nd byte
            return Int(byteArray[1])
        } else {
            // Heart Rate Value Format is in the 2nd and 3rd bytes
            return (Int(byteArray[1]) << 8) + Int(byteArray[2])
        }
    }
    
    
    //MARK: Function para obtener los pasos que vienen del sensor
    private func enableStepsNotification(from characteristic: CBCharacteristic) {
        let ArrayToSend: [UInt8] = [0xFF, 0x20, 0x06, 0x00, 0x02, 0x27]
        //var integerToSend = NSInteger(0xFF2006000227)
        //var integerToSend = NSInteger(0x2702000620FF)
        //var int: UInt64 = 0xFF2006000227
        //var ArrayToSend = [0x27, 0x02, 0x00, 0x06, 0x20, 0xFF]
        let data = Data(ArrayToSend)
    
        let stringBytes = String(data: data, encoding: .utf8)
        print("Bytes : \(stringBytes)")
        heartRatePeripheral.writeValue(data, for: characteristic, type: .withoutResponse)
    }
    
    
    //MARK: Funcion para obtener los bytes de una caracteristica
    private func validateAcknowledgeResponse(from characteristic: CBCharacteristic){
        guard let characteristicData = characteristic.value else { return }
        let byteArray = [UInt8](characteristicData)
        let acknowledge_response: [UInt8] = [255, 0, 12, 0, 2, 5, 16, 1, 1, 1, 1, 38]
        print("Array convertido : \(byteArray)")
        
        if byteArray == acknowledge_response {
            print("AQUI_EJECUTAMOS_WRITE")
            enableStepsNotification(from: caract_common_write)
        }else{
            if validateStepsHeader(array: byteArray) == true {
                //viene info de pasos
                let factor = 255
                
                let byte10 = Int(byteArray[10]) & factor
                let byte11 = Int(byteArray[11]) & factor
                let byte12 = Int(byteArray[12]) & factor
                
                print("Bytes : \(byte10) \(byte11) \(byte12)")
                print("Bytes Dezplazados : \(byte10 << 16 ) \(byte11 << 8) \(byte12)")

                
                let steps = (byte10 << 16) + (byte11 << 8) + (byte12)
                print("Pasos: \(steps) ")
                
                if firstLecture == false {
                    firtStepsLecture = steps
                    firstLecture = true
                    print("Cero pasos primera lectura con \(firtStepsLecture)")
                }else{
                    print("Pasos_F_LECTURE: \(firtStepsLecture)")
                    print("Pasos_SESSION : \(steps - firtStepsLecture) ")
                }
            }
        }
    }
    
    //MARK: Funcion para validar el header de las lecturas
    private func validateStepsHeader(array : [UInt8]) -> Bool {
            
        let expected_header:Array<UInt8>.SubSequence = [0xFF, 0x00, 0x0F, array[3], array[4], 0x01, 0x10, 0x02, 0x00, 0x04]
        let received_header = array.prefix(10)
        if expected_header == received_header {
            return true
        }else{
            
            return false
        }
    }
    

    
}//END Extension Peripherial Delegate



//Para ver la documentación de BT UUID
/*
 https://www.bluetooth.com/xml-viewer/?src=https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Services/org.bluetooth.service.heart_rate.xml
 */

//Para ver los ID de las características
/*
 https://www.bluetooth.com/specifications/gatt/characteristics/
 */

//Para ver la documentacion de CBCaracteristics
/*
 https://developer.apple.com/documentation/corebluetooth/cbcharacteristicproperties
 */


